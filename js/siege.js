class Siege {

    constructor(width, height) {
        this.engine = Matter.Engine.create();
        this.world = this.engine.world;
        this.world.bodies = [];
        this.width = width;
        this.height = height;
        this.render = Matter.Render.create({
            element: document.body,
            engine: this.engine,
            options: {
                width: this.width,
                height: this.height,
                background: 'none',
                wireframes: false
            }
        });
        Matter.Render.lookAt(this.render, {
            min: { x: 0, y: 0 },
            max: { x: this.width, y: this.height }
        });
        this.runner = Matter.Runner.create();
        this.brickSize = 50;
        this.bulletSpeedSpan = document.getElementById('bulletSpeed');
        this.brickImage = './img/stone.png';
        this.brickDraggedImage = './img/dragStone.png';
        this.cannonImage = './img/cannon.png';
        this.inertia = null;
        this.bulletSpeed = 10;

        this.addFloor();
        this.addGround();
        this.addCannon();
        this.addMouse();
    }

    addFloor() {
        let floor = document.getElementById('floor');
        floor.setAttribute("style",`width: ${this.width}px; height: ${this.height}px`);
    }

    addGround() {
        Matter.World.add(this.world, [
            //bottom
            Matter.Bodies.rectangle(this.width/2, this.height+30, this.width, 80, {isStatic: true, render: {fillStyle: '#8B4513'}}),
             //left
            Matter.Bodies.rectangle(-15, this.height/2, 30, this.height, {isStatic: true}),
            //right
            Matter.Bodies.rectangle(this.width+15, this.height/2, 30, this.height, {isStatic: true})
        ]);
    }

    addCannon() {
        this.cannon = Matter.Bodies.rectangle(this.width-5, this.height-20, 304, 62, {
            isStatic: true,
            angle: Math.PI/5,
            render: {
                sprite: {
                    xScale: 1,
                    yScale: 1,
                    texture: this.cannonImage
                }
            }
        });
        Matter.World.add(this.world, [this.cannon]);
    }

    addMouse() {
        let mouse = Matter.Mouse.create(this.render.canvas);
        this.mouseConstraint = Matter.MouseConstraint.create(this.engine, {
                                mouse: mouse,
                                constraint: {
                                    stiffness: 1,
                                    length: 0,
                                    angularStiffness: 0,
                                    render: {
                                        visible: false
                                    }
                                }
        });
        this.render.mouse = mouse;
        Matter.Events.on(this.mouseConstraint, "startdrag", e => {
            let b = e.body;
            if (b.label !== 'brick') {
                return;
            }
            b.render.sprite.texture = this.brickDraggedImage;
            this.inertia = b.inertia;
            this.setBrickStatic(true);
            Matter.Body.setStatic(b, false);
            Matter.Body.setInertia(b, Infinity);
        });
        Matter.Events.on(this.mouseConstraint, "enddrag", e => {
            let b = e.body;
            if (b.label !== 'brick') {
                return;
            }
            this.setBrickStatic(false);
            Matter.Body.setInertia(b, this.inertia);
            b.render.sprite.texture = this.brickImage;
        });
        Matter.World.add(this.world, [this.mouseConstraint]);
    }

    start() {
        console.log('Start app');
        this.addBricks();
        this.addKeyboard();
        Matter.Render.run(this.render);
        Matter.Runner.run(this.runner, this.engine);
    }

    stop() {
        Matter.Render.stop(this.render);
        Matter.Runner.stop(this.runner);
    }

    addBricks() {
        let brickSize = this.brickSize,
            columns = 9,
            rows = 3,
            boxes = [],
            options = {
                label: 'brick',
                density: 1000,
                friction: 1.0,
                render: {
                    sprite: {
                        xScale: brickSize/32,
                        yScale: brickSize/32,
                        texture: this.brickImage
                    }
                }
            };
        this.stack = Matter.Composites.stack(this.width/2, this.brickSize*3, columns, rows, 0, 0, (sx, sy) => {
            let x = this.width/4 + Math.round(Matter.Common.random(-this.brickSize*2, this.brickSize*2));
            let y = 0;
            return Matter.Bodies.rectangle(x, y, brickSize, brickSize, {...options});
        });
        Matter.World.add(this.world, [this.stack]);
    }

    addKeyboard() {
        document.addEventListener('keydown', e => {
            console.log(e.keyCode);
            switch (e.keyCode) {
            case 83: //s
                console.log('bullet');
                this.shoot()
                break;
            case 38: //up
                var ang = Math.min(this.cannon.angle + 0.02, 1.47831);
                Matter.Body.setAngle(this.cannon, ang);
                break;
            case 40: //down
                var ang = Math.max(this.cannon.angle - 0.02, 0);
                Matter.Body.setAngle(this.cannon, ang);
                break;
            case 65: //a
                this.updateBulletSpeed(-5);
                break;
            case 68: //d
                this.updateBulletSpeed(5);
                break;
            }
        });
    }

    setBrickInertia(brick, inertia) {
        if (inertia) {
            brick._original = {
                inertia: brick.inertia
            }
            Matter.Body.setInertia(brick, inertia);
        } else if (brick._original) {
            Matter.Body.setInertia(brick, brick._original.inertia);
        }
    }

    setBrickStatic(isStatic) {
        this.stack.bodies.forEach(brick => {
            Matter.Body.setStatic(brick, isStatic);
        });
    }

    setModeIndicator(cl) {
        this.modeIndicator.setAttribute('class', cl);
    }

    shoot() {
        if (this.bullet) {
            Matter.Composite.remove(this.world, this.bullet);
        } else {
            this.bullet = Matter.Bodies.circle(this.width-5, this.height-20, 15, {
                density: 3,
                render: {
                    fillStyle: 'black'
                }
            });
        }
        let cannonAngle = this.cannon.angle;
        let cannonVec = {
            x: -Math.cos(cannonAngle),
            y: -Math.sin(cannonAngle)
        }
        Matter.Body.setPosition(this.bullet, {
            x: (this.width-5) + cannonVec.x*281,
            y: (this.height-20) + cannonVec.y*281
        });
        console.log(`pos: ${this.bullet.position.x}, ${this.bullet.position.y}`);
        let velocity = {
            x: cannonVec.x*this.bulletSpeed,
            y: cannonVec.y*this.bulletSpeed
        };
        console.log(`vel: ${velocity.x}, ${velocity.y}`);
        Matter.Body.setVelocity(this.bullet, velocity);
        Matter.World.add(this.world, [this.bullet]);
    }

    updateBulletSpeed(diff) {
        this.bulletSpeed = Matter.Common.clamp(this.bulletSpeed + diff, 5, 80);
        this.bulletSpeedSpan.textContent = this.bulletSpeed;
    }

}
